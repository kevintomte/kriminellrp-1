DarkRP.disabledDefaults["modules"] = {
	["afk"]              = true,
	["chatsounds"]       = false,
	["events"]           = false,
	["fpp"]              = false,
	["f1menu"]           = false,
	["f4menu"]           = true,
	["hitmenu"]          = false,
	["hud"]              = true,
	["hungermod"]        = true,
	["playerscale"]      = false,
	["sleep"]            = false,
	["fadmin"]           = false,
}

DarkRP.disabledDefaults["jobs"] = {
	["chief"]     = true,
	["citizen"]   = true,
	["cook"]      = true,
	["cp"]        = true,
	["gangster"]  = true,
	["gundealer"] = true,
	["hobo"]      = true,
	["mayor"]     = true,
	["medic"]     = true,
	["mobboss"]   = true,
}

DarkRP.disabledDefaults["shipments"] = {
	["AK47"]         = true,
	["Desert eagle"] = true,
	["Fiveseven"]    = true,
	["Glock"]        = true,
	["M4"]           = true,
	["Mac 10"]       = true,
	["MP5"]          = true,
	["P228"]         = true,
	["Pump shotgun"] = true,
	["Sniper rifle"] = true,
}

DarkRP.disabledDefaults["entities"] = {
	["Drug lab"]      = true,
	["Gun lab"]       = true,
	["Money printer"] = true,
	["Microwave"]     = true,
}

DarkRP.disabledDefaults["vehicles"] = {}

DarkRP.disabledDefaults["food"] = {
	["Banana"]           = true,
	["Bunch of bananas"] = true,
	["Melon"]            = true,
	["Glass bottle"]     = true,
	["Pop can"]          = true,
	["Plastic bottle"]   = true,
	["Milk"]             = true,
	["Bottle 1"]         = true,
	["Bottle 2"]         = true,
	["Bottle 3"]         = true,
	["Orange"]           = true,
}

DarkRP.disabledDefaults["doorgroups"] = {
	["Cops and Mayor only"] = true,
	["Gundealer only"]      = true,
}

DarkRP.disabledDefaults["ammo"] = {
	["Pistol ammo"]  = true,
	["Rifle ammo"]   = true,
	["Shotgun ammo"] = true,
}

DarkRP.disabledDefaults["agendas"] = {
	["Gangster's agenda"] = false,
	["Police agenda"] = false,
}

DarkRP.disabledDefaults["groupchat"] = {
	[1] = true,
	[2] = true,
	[3] = true,
}

DarkRP.disabledDefaults["hitmen"] = {
	["mobboss"] = true,
}

DarkRP.disabledDefaults["demotegroups"] = {
	["Cops"]		 = false,
	["Gangsters"]	 = false,
}
