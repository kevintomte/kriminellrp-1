freshChat['config'] = {}
freshChat['config']['width'] = 680
freshChat['config']['height'] = 350

freshChat['config']['offsetX'] = 0
freshChat['config']['offsetY'] = 0

freshChat['config']['webEmoteLimit'] = 5
freshChat['config']['chatMessageTimeLimit'] = 0
freshChat['config']['canBypassChatSpam'] = function( ply )
	return ply:IsAdmin() or ply:IsSuperAdmin()
end
freshChat['config']['chatR9KMode'] = false
freshChat['config']['playerConnectionMessages'] = true
freshChat['config']['playerConnectedMessage'] = function( ply )
	return string.format( "%s har anslutit till servern!", ply:Nick(), ply:SteamID() )
end
freshChat['config']['playerDisconnectedMessage'] = function( ply )
	return string.format( "%s har loggat ut.", ply:Nick(), ply:SteamID() )
end
freshChat['config']['canAddChatRoom'] = function( ply )
	return true
end
freshChat['config']['chatPreviewDuration'] = 25
freshChat['config']['defaultRooms'] = {
	{
		name = "Lokal",
		accessList = {},
		canAccess = function( ply )
			return true 
		end
	},
	{
		name = "Admin",
		accessList = {},
		canAccess = function( ply )
			return ply:IsAdmin()
		end
	}
}
freshChat['config']['defaultAccessToRooms'] = function( ply )
	return ply:IsAdmin()
end
freshChat['config']['chatTags'] = function( ply )
	if not ply then return "" end
	if ply:IsAdmin() then
		return "ADMIN" end

	if ply:IsSuperAdmin() then
		return "SUPERADMIN" end
	return ""
end
freshChat['config']['canRemoveMessages'] = function( ply )
	return ply:IsSuperAdmin()
end
freshChat['config']['emotes'] = {
	{ ":)", Material("icon16/emoticon_smile.png") },
	{ ":D", Material("icon16/emoticon_grin.png") },
	{ ">:D", Material("icon16/emoticon_evilgrin.png") },
	{ "8D", Material("icon16/emoticon_happy.png") },
	{ ":O", Material("icon16/emoticon_surprised.png") },
	{ ":0", Material("icon16/emoticon_surprised.png") },
	{ ":o", Material("icon16/emoticon_surprised.png") },
	{ ":P", Material("icon16/emoticon_tongue.png") },
	{ ":p", Material("icon16/emoticon_tongue.png") },
	{ ":(", Material("icon16/emoticon_unhappy.png") },
	{ ":3", Material("icon16/emoticon_waii.png") },
	{ ";)", Material("icon16/emoticon_wink.png") },
	{ ";D", Material("icon16/emoticon_wink.png") },
	{ ":illuminati:", Material("icon16/eye.png") },
	{ ":football:", Material("icon16/sport_football.png") },
	{ ":basketball:", Material("icon16/sport_basketball.png") },
	{ ":golf:", Material("icon16/sport_golf.png") },
	{ ":raquet:", Material("icon16/sport_raquet.png") },
	{ ":soccer:", Material("icon16/sport_soccer.png") },
	{ ":tennis:", Material("icon16/sport_tennis.png") },
	{ ":star:", Material("icon16/star.png") },
	{ ":car:", Material("icon16/car.png") },
	{ ":world:", Material("icon16/world.png") },
	{ ":cake:", Material("icon16/cake.png") },
	{ ":linux:", Material("icon16/tux.png") },
	{ ":bug:", Material("icon16/bug.png") },
	{ ":money:", Material("icon16/money.png") },
	{ "$", Material("icon16/money_dollar.png") },
	{ "<3", Material("icon16/heart.png") },
	{ "<|", Material("icon16/ruby.png") },
	{ "->", Material("icon16/arrow_right.png") },
	{ "<-", Material("icon16/arrow_left.png") },
	{ ":cry:", "http://178.32.177.208/gmod/images/emojis/cry.gif", 350, 198 },
	{ ":finessed:", "http://178.32.177.208/gmod/images/emojis/finessed.gif", 384, 219 },
	{ ":gj:", "http://178.32.177.208/gmod/images/emojis/gj.gif", 300, 225 },
	{ ":booty:", "http://178.32.177.208/gmod/images/emojis/booty.gif", 310, 203 },
	{ ":facepalm:", "http://178.32.177.208/gmod/images/emojis/facepalm.gif", 200, 150 },
	{ ":ok:", "http://178.32.177.208/gmod/images/emojis/ok.gif", 200, 200 },
	{ ":cyrus:", "http://178.32.177.208/gmod/images/emojis/cyrus.gif", 260, 146 },
}
freshChat['config']['colors'] = {
	{ "^1", Color(239, 72, 54) }, // red
	{ "^2", Color(89, 171, 227) }, // light blue
	{ "^3", Color(31, 58, 147) }, // blue
	{ "^4", Color(1, 152, 117) }, // green
	{ "^5", Color(249, 105, 14) }, // orange
	{ "^6", Color(142, 68, 173) }, // purple
	{ "^7", Color(190, 144, 212) }, // pink
	{ "^8", Color(171, 183, 183) }, // gray
	{ "^9", Color(0, 0, 0, 200) }, // default
	{ "^0", Color(0, 0, 0) } // black
}
freshChat['config']['wordFiltering'] = false
freshChat['config']['wordFilterList'] = {}
freshChat['config']['wordFilterCharacters'] = { 
	"%", "$", "#", "&", "!", "?", "@", "/" 
}