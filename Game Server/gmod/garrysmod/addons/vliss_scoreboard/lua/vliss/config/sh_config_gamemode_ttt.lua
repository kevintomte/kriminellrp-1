-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ TTT MODE SETTINGS ]
-----------------------------------------------------------------
-- These settings are for the TTT GAMEMODE ONLY.
-- If you are using Vliss on another gamemode, then you should 
-- set Vliss.TTT.Enabled = false
-----------------------------------------------------------------

Vliss.TTT.Enabled = false -- Set this to true if TTT is your gamemode.

Vliss.TTT.ColorDetective = Color(25, 25, 200, 200)
Vliss.TTT.ColorTraitor = Color(200, 25, 25, 200)
Vliss.TTT.ColorTerrorist = Color(25, 200, 25, 200)
Vliss.TTT.ColorMIA = Color(130, 190, 130, 200)
Vliss.TTT.ColorDead = Color(130, 170, 10, 200)
Vliss.TTT.ColorSpec = Color(200, 200, 0, 200)

Vliss.TTT.ShowUsedTeamsOnly = true -- Set this to true if you want only the teams that players are in to be shown in TTT.

Vliss.TTT.RemainingTimeEnabled = true
Vliss.TTT.RemainingTimeText = Color( 255, 255, 255, 255 )

