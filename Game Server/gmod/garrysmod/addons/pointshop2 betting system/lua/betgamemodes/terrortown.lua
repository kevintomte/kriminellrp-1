--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Trouble in Terrorist Town (TTT) gamemode

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = {2,"Traitor", Color(255, 77, 77), function(ply) return ply:GetRole() == ROLE_TRAITOR end}
BETTING.SecondTeam = {3,"Innocent", Color(0, 204, 68), function(ply) return ply:GetRole() == ROLE_INNOCENT or ply:GetRole() == ROLE_DETECTIVE end}
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanBetTTT(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if (GetRoundState() != ROUND_ACTIVE and GetRoundState() != ROUND_PREP) then return false,"You can't bet until the new round starts..." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetTTT",PlayerCanBetTTT)

local function AllowBetsWhenRoundPrepares()
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
hook.Add("TTTPrepareRound","AllowBetsWhenRoundPrepares",AllowBetsWhenRoundPrepares)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
local function TTTEndRoundBets(result)
	if not result then return end
	if (result == WIN_NONE) then BETTING.FinishBets(result, true)
	elseif (result == WIN_TIMELIMIT) then BETTING.FinishBets(WIN_INNOCENT)
	else BETTING.FinishBets(result) end
end
hook.Add("TTTEndRound","TTTEndRoundBets",TTTEndRoundBets)