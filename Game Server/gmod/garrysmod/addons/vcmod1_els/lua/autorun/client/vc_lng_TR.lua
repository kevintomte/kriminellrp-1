// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Türkçe"
Lng.Language_Code = "TR"
Lng.Translated_By_Name = "Just."
Lng.Translated_By_Link = "http://steamcommunity.com/id/Justty/"
Lng.Translated_Date = "2015 03 17"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Aracýnýz kilitli."
Lng.UnLocked = "Aracýnýz kilitsiz."
Lng.Chat = 'Eðer ayarlarý deðiþtirmek istiyorsan (araba ýþýklandýrma, bakýþ açýsý) gibi sohbet kutusuna "!vcmod" yaz.'
Lng.Broken = "Araban bozuldu.Tamire ihtiyacý var."
Lng.Trl_Atch = "Römörk takýldý."
Lng.Trl_Detch = "Römörk çýkarýldý."
Lng.ELS_TuningIntoPoliceC = "Polis radyosuna baðlanýlýyor"
Lng.ELS_NoPoliceRCFound = "Hiç bir polis radyosu aktif deðil."

////////Pickup
Lng.TouchCar100 = "Arabanı tamamen tamir etmek için beni arabaya dokundur."
Lng.TouchCar25 = "Arabanın %25'ini tamir etmek için beni arabaya dokundur."
Lng.TouchCar10 = "Arabanın %10'sini tamir etmek için beni arabaya dokundur."

////////MENU general
Lng.Info = "Bilgilendirme"
Lng.Menu = "Menü"
Lng.Language = "Dil"
Lng.Personal = "Kiþisel"
Lng.Administrator = "Yönetici"
Lng.Options = "Ayarlar"
Lng.ELSOptions = "ELS Ayarlarý"
Lng.Main = "Ana"
Lng.Controls = "Kontroller"
Lng.HUD = "HUD"
Lng.View = "Bakýþ Açýsý"
Lng.Radio = "Radyo"
Lng.Multiplier = "Çoðaltýcý"
Lng.OptOnly_You = "Bu ayarlar sadece seni etkiliyecektir."
Lng.OptOnly_Admin = "Bu ayarlar sadece yetkili bir kiþi tarafýndan deðiþtirilebilir."
Lng.NPC_Settings = "NPC ayarlarý"
Lng.Height = "Yükseklik"
Lng.FadeOutDistance = "Kararma mesafesi"
Lng.Reset = "Yeniden Ayarla"
Lng.Save = "Kaydet"
Lng.Load = "Yükle"
Lng.Volume = "Ses"
Lng.Distance = "Uzaklýk"
Lng.None = "Hiçbiri"
Lng.EnterKey = "Bir tuþa bas"

Lng.Enabled_Cl = "clientside Etkin!"
Lng.Enabled_Sv = "serverside Etkin!"

Lng.Lights = "Iþýklandýrma"
Lng.Health = "Can"
Lng.Sound = "Ses"
Lng.Other = "Diðer"
Lng.CreatedBy = "Bu modun yaratýcýsý"
Lng.Enabled = "Etkin"
Lng.OffTime = "Zamaný Kapat"
Lng.Time = "Zaman"
Lng.DistMultiplier = "Uzaklýk Çarpaný"
Lng.ControlsReset = "Kontroller yeniden ayarlandý."
Lng.SettingsSaved = "Ayarlar kaydedildi."
Lng.SettingsReset = "Ayarlar yeniden ayarlandý."
Lng.LoadedSettingsFromServer = "Ayarlar sunucudan yüklendi."
Lng.InteriorIndicators = "Ýç göstergeler"
Lng.ExtraGlow = "Ekstra parýltý"

////////MENU ELS
Lng.ManulSiren = "Manuel Siren"
Lng.ELS_SirenSwitch = "ELS siren anahtarý"
Lng.ELS_SirenToggle = "ELS sireni sabitle"
Lng.ELS_LightsSwitch = "ELS ýþýklandýrma anahtarý"
Lng.ELS_LightsToggle = "ELS ýþýklandýrmayý sabitle"
Lng.VCModMainEnabled = "VCMod etkin!"
Lng.VCModELSEnabled = "VCMod ELS etkin!"
Lng.ELSLightsEnabled = "ELS ýþýklandýrmasý etkin!"
Lng.AutoDisableELSLights = "Otomatik olarak ELS ýþýklandýrmasýný kapalý tut."
Lng.OffOnExit = "Çýkýþta kapat."
Lng.Siren = "Siren"
Lng.AutoDisableELSSounds = "Otomatik olarak ELS seslendirmesini kapalý tut."
Lng.Manual = "Manuel"
Lng.Bullhorn = "Borozan"
Lng.PoliceChatterEnabled = "Polis telsizi etkin!"
Lng.PoliceChatter = "Polis telsizi"
Lng.PoliceChatter_Info = "Polis telsizi polislerin aralarýndaki iletiþimi bir baþka boyuta taþýr."
Lng.SelectedRadioChatter = "Polis telsizi radyosu frekansý seçildi!"
Lng.ReduceDamageToEmergencyVehicles = "Devlet araçlarýna gelen hasarý azalt."

////////MENU personal info
Lng.YouAreUsingVCMod = "VCMod kullanýyorsun."
Lng.ServerIsUsingVCMod = "Bu sunucu VCMod kullanýyor."
Lng.Info_EverThought = "Çoðu zaman gmod'daki arabalardan sýkýlýyordun deðilmi?\nVCMod gmod'daki arabalarý yarýþ oyunlarýndaki gibi güzel yapmak için var!."
Lng.Info_VCModHasFollowingAddons = "VCMod aþaðýda belirtilen eklentilere sahip:"

////////MENU personal options
Lng.VisDist = "Görüþ uzaklýðý"
Lng.Warmth = "Yumuþatýcý"
Lng.Lines = "Çizgiler"
Lng.Glow = "Parýltý"
Lng.DynamicLights = "Dinamik ýþýklandýrma"
Lng.ThirdPView = "3.Kiþi bakýþ açýsý"
Lng.DynamicView = "Dinamik bakýþ"
Lng.AutoFocus = "Otomatik odak"
Lng.Reverse = "Geri çevir"
Lng.VectorStiffness = "Vektör sertliði %"
Lng.AngleStiffness = "Açý sertliði %"
Lng.IgnoreWorld = "Dünyayý görmezden gel"
Lng.TruckView = "Kamyon römörk bakýþ açýsý"

////////MENU controls
Lng.HoldDuration = "Durma süresi"
Lng.Mouse = "Maus"
Lng.KeyboardInput = "Klavye giriþi"
Lng.MouseInput = "Maus giriþi"

Lng.NightLights = "Gece lambasý"
Lng.HeadLights = "Ana lambalar"
Lng.LowHigh = "Düþük/Yüksek ýþýk anahtarý"
Lng.HazardLights = "Tehlike ýþýklandýrmasý"
Lng.BlinkerLeft = "Sol sinyal"
Lng.BlinkerRight = "Sað sinyal"
Lng.Horn = "Korna"
Lng.Cruise = "Seyir"
Lng.LockUnlock = "Kilitli/Unlock"
Lng.LookBehind = "Arkaya bak"
Lng.DetachTrl = "Römörkü aç"

////////MENU hud
Lng.Effect3D = "3D efekti"
Lng.HUDHeight = "Yan HUD Uzunluðu %"
Lng.HUD_Name = "Ýsim"
Lng.HUD_Icons = "Ýkonlar"
Lng.HUD_Cruise = "Seyir"
Lng.HUD_Cruise_MPH = "mi/h yerine km/h"
Lng.HUD_Repair = "Tamirat"
Lng.HUD_ELS_Siren = "ELS Siren"
Lng.HUD_ELS_Lights = "ELS Iþýklandýrma"

////////MENU admin options
Lng.HandbrakeLights = "El freni ýþýklandýrmasý"
Lng.InteriorLights = "Ýç ýþýklandýrma"
Lng.BlinkersOffExit = "Sinyal (sinyalleri aç kapa)"

Lng.DamageEnabled = "Hasar AÇIK!"
Lng.StartHealthMultiplier = "Can çarpanýný aktive et."
Lng.PhysicalDamage = "Fiziksel hasar"
Lng.FireDuration = "Yangýn süresi"
Lng.RemoveCarAfterExplosion = "Arabayý patlamadan sonra sil."
Lng.ReducePlayerDmgWhileInCar = "Arabanýn içindeki oyuncunun alýcaðý hasarý düþür."

Lng.DoorSounds = "Kapý sesleri"
Lng.TruckRevBeep = "Römörk sesi"

Lng.SteeringWheelLOnExit = "Çýkýþta direksiyonu kitle."
Lng.BrakesLOnExit = "Çýkýþta el frenini çek."
Lng.WheelDust = "Patinaj dumaný çýksýnmý?"
Lng.WheelDustWhileBraking = "Fren'e basarken duman çýksýnmý?"
Lng.MatchPlayerSpeedExit = "Çýkýþta aracýn hýzý oyuncunun hýzý ile ayný olsunmu?"
Lng.NoCollidePlyOnExit = "Çýkýþta insanlar aracýndan geçebilsinlermi?"
Lng.Exhaust = "Egzoz"
Lng.PassengerSeats = "Yolcu koltuklarý"
Lng.TrailerAttach = "Römörk takýldý"
Lng.TrailerAttachConStrengthM = "Römörk baðlantý gücü çarpaný"
Lng.TrailersCanAtchToReg = "Römörk normal araçlara baðlanýlabilir"
Lng.RepairToolSpeedMult = "Tamir aracý hýzý çarpaný"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng