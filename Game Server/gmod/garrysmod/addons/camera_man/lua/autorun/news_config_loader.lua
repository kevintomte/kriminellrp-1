--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

include("news_config.lua")

if (SERVER) then
	AddCSLuaFile("news_config.lua")
end

MsgC(Color(0, 255, 0), "Loaded DarkRP News Camera system by Chessnut.\n")