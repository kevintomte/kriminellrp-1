CreateClientConVar("surrender_shout_key","17")

hook.Add("CalcView","surrender_firstperson_view",function(ply, pos, angles, fov)
	if ply:IsSurrendering() or ply:IsCableTied() then
		local attachid = ply:LookupAttachment("eyes")
		if attachid == 0 then attachid = ply:LookupAttachment("1") end
		if ply:GetAttachment(attachid) then
			local view = {}
			if ply:IsCableTied() then
				view.origin = ply:GetAttachment(attachid).Pos + ply:GetAttachment(attachid).Ang:Forward() * 5 + ply:GetAttachment(attachid).Ang:Right() * -10
			else
				view.origin = ply:GetAttachment(attachid).Pos + ply:GetAttachment(attachid).Ang:Forward() * 5
			end
			if ply:IsCableTied() then
				view.angles = ply:GetAttachment(attachid).Ang
			else
				view.angles = angles
			end
			view.fov = fov

			return view
		end
	end
end)

hook.Add("ShouldDrawLocalPlayer","surrender_shoulddraw_localplayer",function(ply)
	if LocalPlayer():IsSurrendering() or LocalPlayer():IsCableTied() then
		return true
	end
end)

hook.Add("PlayerBindPress","surrendering_disable_movements",function(ply,bind,pressd)
	if ply:IsSurrendering() or ply:IsCableTied() then
		if string.find(bind,"+jump") or string.find(bind,"+duck") then
			return true
		end
	end
end)

local sur_chat_open = false
hook.Add("StartChat","surrender_onchat_open",function() sur_chat_open = true end)
hook.Add("FinishChat","surrender_onchat_close",function() sur_chat_open = false end)

hook.Add("canRequestHit","surrender_prevent_hitmenu",function(hitman) if hitman:IsSurrendering() or hitman:IsCableTied() then return false, "Player is surrendering/tied" end end)

local shout_key_pressed = false
hook.Add("Think","surrender_shout_button",function()
	if not shout_key_pressed and not sur_chat_open and not gui.IsGameUIVisible() then
		if input.IsKeyDown(GetConVarNumber("surrender_shout_key")) then
			shout_key_pressed = true
			RunConsoleCommand("surrender_shout")
		end
	else
		if not input.IsKeyDown(GetConVarNumber("surrender_shout_key")) then
			shout_key_pressed = false
		end
	end
end)

hook.Add("HUDDrawTargetID","surrender´_hint",function()
	if LocalPlayer():IsCableTied() or LocalPlayer():IsSurrendering() then return end
    local tr = util.GetPlayerTrace( LocalPlayer() )
    local trace = util.TraceLine( tr )
    if (!trace.Hit) then return end
    if (!trace.HitNonWorld) then return end
	if LocalPlayer():GetShootPos():Distance(trace.HitPos) > 100 then return end
	if not trace.Entity:IsPlayer() then return end
	
    if trace.Entity:IsCableTied() or trace.Entity:IsSurrendering() then
		draw.DrawText("Press USE to to interact","ChatFont",ScrW() / 2,ScrH() / 1.5,Color(200,200,200,255),TEXT_ALIGN_CENTER)
    end
end)

usermessage.Hook("surrender_shout_viewmodel",function(um)
	local shout_viewmodel_controller = true
	local shout_viewmodel_multi = 0
	local shout_pos = Vector(8.586, -6.018, 2)
	local shout_ang = Vector(-28.142, 36.583, -30.251)
	hook.Add("CalcViewModelView","surrender_shout_viewmodel",function(wep,vm,oldpos,oldang,pos,ang)
		if ( !IsValid( wep ) ) then return end
		local vm_origin, vm_angles = pos, ang
		-- Controls the position of all viewmodels
		local func = wep.GetViewModelPosition
		if ( func ) then
			local pos, ang = func( wep, pos*1, ang*1 )
			vm_origin = pos or vm_origin
			vm_angles = ang or vm_angles
		end
		-- Controls the position of individual viewmodels
		func = wep.CalcViewModelView
		if ( func ) then
			local pos, ang = func( wep, vm, oldpos*1, oldang*1, pos*1, ang*1 )
			vm_origin = pos or vm_origin
			vm_angles = ang or vm_angles
		end
	
		if shout_viewmodel_controller then
			if shout_viewmodel_multi >= 0.96 then
				shout_viewmodel_controller = false
			end
			shout_viewmodel_multi = Lerp(0.06,shout_viewmodel_multi,1)
		else
			if shout_viewmodel_multi <= 0.04 then
				hook.Remove("CalcViewModelView","surrender_shout_viewmodel")
			end
			shout_viewmodel_multi = Lerp(0.05,shout_viewmodel_multi,0)
		end
		
		vm_angles:RotateAroundAxis(vm_angles:Right(), shout_ang.x * shout_viewmodel_multi)
		vm_angles:RotateAroundAxis(vm_angles:Up(), shout_ang.y * shout_viewmodel_multi)
		vm_angles:RotateAroundAxis(vm_angles:Forward(), shout_ang.z * shout_viewmodel_multi)
		vm_origin = vm_origin + shout_pos.x * vm_angles:Right() * shout_viewmodel_multi
		vm_origin = vm_origin + shout_pos.y * vm_angles:Forward() * shout_viewmodel_multi
		vm_origin = vm_origin + shout_pos.z * vm_angles:Up() * shout_viewmodel_multi

		return vm_origin, vm_angles
	end)
end)

local surrender_interact_text = [[This player is surrendering.
You can tie them down
to stop them from moving 
for 10 minutes
You can also take any 
weapons they may have
]]
local surrender_interact_text_tied = [[This player is tied up.
You can remove their ties to 
allow them to move
You can also take any 
weapons they may have
]]
function Surrender_Interact_Menu(um)
local surrenderer = um:ReadEntity()

local Interact_Frame = vgui.Create("DFrame")
Interact_Frame:SetSize(200,200)
Interact_Frame:SetTitle("Interact with "..surrenderer:GetName())
Interact_Frame:SetVisible( true )
Interact_Frame:SetDraggable( false )
Interact_Frame:ShowCloseButton( false )
Interact_Frame:Center()
Interact_Frame:SetSkin(GAMEMODE.Config.DarkRPSkin)
Interact_Frame:MakePopup()
Interact_Frame.Think = function()
	if not IsValid(surrenderer) or not surrenderer:IsSurrendering() and not surrenderer:IsCableTied() or LocalPlayer():GetPos():Distance(surrenderer:GetPos()) > 100 then
		Interact_Frame:Close()
	end
end

local Label = vgui.Create("DLabel",Interact_Frame)
Label:SetPos(10,30)
if surrenderer:IsCableTied() then
	Label:SetText(surrender_interact_text_tied)
else
	Label:SetText(surrender_interact_text)
end

	local CableTieButton = vgui.Create("DButton",Interact_Frame )
    CableTieButton:SetPos(20,120)
    CableTieButton:SetSize(160,20)
	if not surrenderer:IsCableTied() then
	CableTieButton:SetText("Tie down")
    CableTieButton.DoClick = function()
		RunConsoleCommand("surrender_cable_tie",tostring(surrenderer:EntIndex()))
		Interact_Frame:Close()
    end
	local cantie, msg = SurrenderSettings["cantie"](LocalPlayer(),surrenderer)
	if cantie == false then
		CableTieButton:SetDisabled(true)
		CableTieButton:SetText("Tie down "..msg or "(Unknown)")
	end
	else
	CableTieButton:SetText("Remove ties")
    CableTieButton.DoClick = function()
		RunConsoleCommand("surrender_uncable_tie",tostring(surrenderer:EntIndex()))
		Interact_Frame:Close()
    end
	end
	
	local StripButton = vgui.Create("DButton",Interact_Frame)
    StripButton:SetPos(20,145)
    StripButton:SetText("Take their weapons")
    StripButton:SetSize(160,20)
    StripButton.DoClick = function()
		RunConsoleCommand("surrender_strip_weapons",tostring(surrenderer:EntIndex()))
		Interact_Frame:Close()
    end
	
	local CloseButton = vgui.Create( "DButton", Interact_Frame )
    CloseButton:SetPos(20,170)
    CloseButton:SetText("Close")
    CloseButton:SetSize(160,20)
    CloseButton.DoClick = function()
		Interact_Frame:Close()
    end
	
Label:SizeToContents()
end
usermessage.Hook("surrender_open_interact_menu",Surrender_Interact_Menu)

function Surrender_Confirm_Dialog()
timer.Simple(0.5,function()
local SurrenderDialog = vgui.Create("DFrame")
SurrenderDialog:SetSize(200, 90 )
SurrenderDialog:SetTitle("Surrender")
SurrenderDialog:SetVisible(true)
SurrenderDialog:SetDraggable(false)
SurrenderDialog:ShowCloseButton(false)
SurrenderDialog:Center()
SurrenderDialog:SetSkin(GAMEMODE.Config.DarkRPSkin)
SurrenderDialog:MakePopup()
SurrenderDialog.Think = function()
	if LocalPlayer():GetVelocity():Length() > 5 or LocalPlayer():Crouching() or not LocalPlayer():OnGround() or not LocalPlayer():Alive() then
		SurrenderDialog:Close()
	end
end

local Label = vgui.Create("DLabel",SurrenderDialog)
Label:SetPos(10,25)
Label:SetText("Do you wish to surrender yourself?\nYou will be frozen for 15 seconds")

	local CloseButton = vgui.Create("DButton",SurrenderDialog)
    CloseButton:SetPos(120,65)
    CloseButton:SetText("No")
    CloseButton:SetSize(60,20)
    CloseButton.DoClick = function()
		SurrenderDialog:Close()
    end
	
	local ConfirmButton = vgui.Create("DButton",SurrenderDialog)
    ConfirmButton:SetPos(20,65)
    ConfirmButton:SetText("Yes")
    ConfirmButton:SetSize(60,20)
    ConfirmButton.DoClick = function()
		SurrenderDialog:Close()
		RunConsoleCommand("surrender_surrender")
    end
	
Label:SizeToContents()
end)
end
usermessage.Hook("surrender_confirm_dialog",Surrender_Confirm_Dialog)