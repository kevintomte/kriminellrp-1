local function FindPlayer( targ )
	local target
	for k, v in pairs( player.GetAll() ) do
		if targ == v:Name() then
			target = v
		end
	end
	return target
end
local function IsHere(targ)
	for k, v in pairs(player.GetAll()) do 
		if v:Nick() == targ or v == targ then 
			return true
		end
	end
	return false
end
util.AddNetworkString("ARS_SendReportToServer")
util.AddNetworkString( "ARS_GotAReport" )
net.Receive("ARS_SendReportToServer", function()
	--MsgN("Done!") --Debug Only
	local tbl = net.ReadTable()
	local NN = tbl.Name
	local SR = tbl.Reason
	local ED = tbl.ExtraDetails
	local RP = tbl.Reporter
	FindPlayer(RP):StartTimeSince()
	if file.Exists( "ars_reports/savedreports.txt", "DATA" ) then
		local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
		local TABLE = util.JSONToTable(FILE)
		AddReport( TABLE, os.date( "%m/%d" ), os.date( "%H:%M:%S" ), RP, team.GetName(FindPlayer(RP):Team()), FindPlayer(RP):GetUserGroup(), FindPlayer(RP):SteamID(), NN, team.GetName(FindPlayer(NN):Team()), FindPlayer(NN):GetUserGroup(), FindPlayer(NN):SteamID(), SR, "", ED )
		timer.Simple(2, function()
			file.Write("ars_reports/savedreports.txt", util.TableToJSON(TABLE))
		end)
	else 
		local ReportingTable = {}
		AddReport( ReportingTable, os.date( "%m/%d" ), os.date( "%H:%M:%S" ), RP, team.GetName(FindPlayer(RP):Team()), FindPlayer(RP):GetUserGroup(), FindPlayer(RP):SteamID(), NN, team.GetName(FindPlayer(NN):Team()), FindPlayer(NN):GetUserGroup(), FindPlayer(NN):SteamID(), SR, "", ED )
		file.Write("ars_reports/savedreports.txt", util.TableToJSON(ReportingTable))
	end
	for k, v in pairs(player.GetAll()) do 
		if v:IsARSAdmin() then 
			net.Start("ARS_GotAReport")
				net.WriteString( RP.." has reported "..NN.." for "..SR.."." )
			net.Send(v)
			ARSNotify(v, 0, 10, RP.." has reported "..NN.." for "..SR..".")
		end
	end
	hook.Call("ARS_PlayerReported", GAMEMODE, RP, NN, SR)
end)



util.AddNetworkString( "ARS_ViewReport_ToServer" )
util.AddNetworkString( "ARS_ViewReport_ToClient" )
net.Receive("ARS_ViewReport_ToServer", function()
	local tbl = net.ReadTable()
	local number = tbl.Number
	local ply = tbl.Player
	if !tbl.close then
		local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
		local TABLE = util.JSONToTable(FILE)
		local ValueTable = CheckTable(TABLE, number)
		net.Start("ARS_ViewReport_ToClient")
			net.WriteTable(ValueTable)
			net.WriteString(number)
			net.WriteBool(false)
		net.Send(FindPlayer(ply))
	else
		local FILE = file.Read( "ars_reports/closed/closedreports.txt", "DATA" )
		local TABLE = util.JSONToTable(FILE)
		local ValueTable = CheckTable(TABLE, number)
		net.Start("ARS_ViewReport_ToClient")
			net.WriteTable(ValueTable)
			net.WriteString(number)
			net.WriteBool(true)
		net.Send(FindPlayer(ply))
	end
end)

util.AddNetworkString( "ARS_DeleteButton" )
net.Receive("ARS_DeleteButton", function()
	local number = net.ReadString()
	local reporter = net.ReadEntity()
	local name = net.ReadString()
	local bool = net.ReadBool()
	if IsHere(reporter) then
		if ARS.SendTypeOfNotification == "Both" then 
			ARSNotify(reporter, 1, 10, "Your report has been closed.")
			reporter:ChatPrint( "Your report has been closed." )
		elseif ARS.SendTypeOfNotification == "Chat" then
			reporter:ChatPrint( "Your report has been closed." )
		elseif ARS.SendTypeOfNotification == "Notification" then
			ARSNotify(reporter, 1, 10, "Your report has been closed." )
		end
	end
	if bool == false then
		local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
		local TABLE = util.JSONToTable(FILE)
		timer.Simple(0.5, function() 
			if table.Count(TABLE) >= 2 then
				table.remove( TABLE, number )
				file.Write("ars_reports/savedreports.txt", util.TableToJSON(TABLE))
			else
				table.Empty(TABLE)
				file.Delete("ars_reports/savedreports.txt")
			end
			ReOpenReports(name)
		end)
		print("Not Closed!")
	elseif bool == true then
		print("Closed!")
		local FILE = file.Read( "ars_reports/closed/closedreports.txt", "DATA" )
		local TABLE = util.JSONToTable(FILE)
		timer.Simple(0.5, function() 
			if table.Count(TABLE) >= 2 then
				table.remove( TABLE, number )
				file.Write("ars_reports/closed/closedreports.txt", util.TableToJSON(TABLE))
			else
				table.Empty(TABLE)
				file.Delete("ars_reports/closed/closedreports.txt")
			end
			if FindPlayer(name):DeleteAble() then
				local HereorNah
				net.Start("ARS_Closed_Logs_ToClient")
					if file.Exists( "ars_reports/closed/closedreports.txt", "DATA" ) then 
						local FILE = file.Read( "ars_reports/closed/closedreports.txt", "DATA" )
						TABLE = util.JSONToTable(FILE)
						HereorNah = true
					else 
						TABLE = {}
						HereorNah = false
					end
					net.WriteTable(TABLE)
					net.WriteBool(HereorNah)
				net.Send(FindPlayer(name))
			end
		end)
	end
end)

util.AddNetworkString("ARS_CompleteReport_ToServer")
net.Receive( "ARS_CompleteReport_ToServer", function()
	local number = net.ReadString()
	local reporter = net.ReadEntity()
	local name = net.ReadString()
	hook.Call( "ARS_CompleteReport", GAMEMODE, number, FindPlayer(name) )
	if IsHere(reporter) then
		if ARS.SendTypeOfNotification == "Both" then 
			ARSNotify(reporter, 1, 10, "Your report has been closed.")
			reporter:ChatPrint( "Your report has been closed." )
		elseif ARS.SendTypeOfNotification == "Chat" then
			reporter:ChatPrint( "Your report has been closed." )
		elseif ARS.SendTypeOfNotification == "Notification" then
			ARSNotify(reporter, 1, 10, "Your report has been closed." )
		end
	end

	local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
	local FILE2 = file.Read( "ars_reports/closed/closedreports.txt", "DATA" )
	local TABLE = util.JSONToTable(FILE)
	if file.Exists( "ars_reports/closed/closedreports.txt", "DATA" ) then
		local TABLE2 = util.JSONToTable(FILE2)
		TABLE2[table.Count(TABLE2)+1] = TABLE[tonumber(number)]
		file.Write("ars_reports/closed/closedreports.txt", util.TableToJSON(TABLE2))
		--print("File Here!")--Debug
		timer.Simple(0.5, function() 
			if table.Count(TABLE) >= 2 then
				table.remove( TABLE, number )
				file.Write("ars_reports/savedreports.txt", util.TableToJSON(TABLE))
			else
				table.Empty(TABLE)
				file.Delete("ars_reports/savedreports.txt")
			end
			ReOpenReports(name)
		end)
	else
		local TABLE2 = {}
		TABLE2[1] = TABLE[tonumber(number)]
		file.Write("ars_reports/closed/closedreports.txt", util.TableToJSON(TABLE2))
		--print("File Not Here!")--Debug
		timer.Simple(0.5, function() 
			if table.Count(TABLE) >= 2 then
				table.remove( TABLE, number )
				file.Write("ars_reports/savedreports.txt", util.TableToJSON(TABLE))
			else
				table.Empty(TABLE)
				file.Delete("ars_reports/savedreports.txt")
			end
			ReOpenReports(name)
		end)
	end
end)


